import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppEditUserComponent } from './app-edit-user/app-edit-user.component';
import { AppLandingComponent, landingChildRoutes } from './app-landing/app-landing.component';
import { AppLoginComponent } from './app-login/app-login.component';
import { AppUsersComponent } from './app-users/app-users.component';

const routes: Routes = [
  {
    path:'',component: AppLoginComponent
  },
  {
    path:'home', component:AppLandingComponent,
    children: landingChildRoutes
  },
  {
    path:'login',component: AppLoginComponent
  },
  {
    path:'users',component:AppUsersComponent
  },
  {
    path:'edit-user',component:AppEditUserComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
