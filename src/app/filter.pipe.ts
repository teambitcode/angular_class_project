import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filter'
})
export class filterPipe implements PipeTransform {
  transform(data:any[],value: number) {
    return data.filter(item=> item.age >= value)
  }
}