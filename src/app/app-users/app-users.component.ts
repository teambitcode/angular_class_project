import { Component, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
 
@Component({
  selector: 'app-app-users',
  templateUrl: './app-users.component.html',
  styleUrls: ['./app-users.component.scss']
})
export class AppUsersComponent implements OnInit {
  modalRef: BsModalRef;
  constructor(private modalService: BsModalService, private router: Router) {}
 
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  ngOnInit(): void {
  }
  navigateWithParameters(){
    this.router.navigate(['/edit-user'], {queryParams:{user_id: "XXX001"}});
  }

}
