import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { AppFooterComponent } from './app-footer/app-footer.component';
import { AppLandingComponent } from './app-landing/app-landing.component';
import { AppUsersComponent } from './app-users/app-users.component';
import { AppRegisterComponent } from './app-register/app-register.component';
import { FormsModule } from '@angular/forms';
import { filterPipe } from './filter.pipe';
import { SideBarComponent } from './side-bar/side-bar.component';
import { AppLoginComponent } from './app-login/app-login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AppEditUserComponent } from './app-edit-user/app-edit-user.component';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    AppFooterComponent,
    AppLandingComponent,
    AppUsersComponent,
    AppRegisterComponent,
    filterPipe,
    SideBarComponent,
    AppLoginComponent,
    AppEditUserComponent
  ],
  imports: [
    ModalModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
