import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-app-login',
  templateUrl: './app-login.component.html',
  styleUrls: ['./app-login.component.scss']
})
export class AppLoginComponent implements OnInit {
  sdasd:string = '';
  isLogin = true;
  suserData:any[] = [];
  constructor(private router: Router,private httpx: HttpClient) {
    this.getAllUsers();
   }
   goToRegister(){
    this.isLogin = false;
   }
   goToLogin(){
    this.isLogin = true;
   }
  ngOnInit(): void {
  }
  login(){
    this.router.navigateByUrl('/home');
  }

  createUser(){
    let tempBody ={
      "email": "teambitcodeVVVVVV@gmail.com",
      "role":"admin",
      "password":"adminadmin",
      "first_name":"Team",
      "last_name":"BitCode"
    };

    this.httpx.post('http://localhost:3000/user/new',tempBody).subscribe(myData => {
      console.log(myData);
      this.getAllUsers();
    });
  }

  getAllUsers() {
    console.log("***************8");
    this.httpx.get('http://localhost:3000/user/getAll').subscribe((data:responceData) => {
      console.log(data.data);
      this.suserData = data.data;
      console.log(data.status);
    });
  }

  updateUser(id){
    let tempBody ={
      "email": "WWWWWWWWWWW@gmail.com",
    };

    this.httpx.patch('http://localhost:3000/user/update/'+id,tempBody).subscribe((myData:responceData) => {
      console.log(myData);
      this.getAllUsers();
    });

  }
}

class responceData {
  data: any;
  status:boolean
}