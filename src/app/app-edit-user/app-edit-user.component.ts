import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-app-edit-user',
  templateUrl: './app-edit-user.component.html',
  styleUrls: ['./app-edit-user.component.scss']
})
export class AppEditUserComponent implements OnInit {
  id = null;
  constructor(private router: ActivatedRoute) {
    this.router.queryParams.subscribe(params => {
      if(params && params.user_id){
        console.log("****************");
        console.log(params.user_id);
        console.log("****************");
        this.id = params.user_id;
      } 
   });
   }

  ngOnInit(){

 }

}
