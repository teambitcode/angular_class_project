import { Component, OnInit } from '@angular/core';
import { Routes } from '@angular/router';
import { AppRegisterComponent } from '../app-register/app-register.component';
import { AppUsersComponent } from '../app-users/app-users.component';

@Component({
  selector: 'app-landing',
  templateUrl: './app-landing.component.html',
  styleUrls: ['./app-landing.component.scss']
})
export class AppLandingComponent implements OnInit {
  value:number = 0;
  isAdmin = false;
  constructor() { }
students :any[] = [
  {"name":"John", "age":"23", "city":"Agra"},
  {"name":"Steve", "age":"28", "city":"Delhi"},
  {"name":"Peter", "age":"32", "city":"Chennai"},
  {"name":"Chaitanya", "age":"28", "city":"Bangalore"}
];
  ngOnInit(): void {
   let idValue =  sessionStorage.getItem('id');
   this.isAdmin = true;
  }
  getStudents():any[]{
    return this.students.filter(item=> item.age >= this.value)
  }

}



export const landingChildRoutes : Routes = [
  {
    path: 'land',
    component: AppUsersComponent
  },
  {
    path: 'edit',
    component: AppRegisterComponent
  }
];