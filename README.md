# To install angular on your PC

npm install -g @angular/cli

## to clone this project
git clone https://gitlab.com/teambitcode/angular_class_project.git

## before run this
run this command

npm install

## to run this
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## to generate component

Run `ng generate component name-of-the-component` to generate a new component.